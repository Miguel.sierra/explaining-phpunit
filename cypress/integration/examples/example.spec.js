describe("Knowledge Base Application", () => {
    it("Shows a placeholder", () => {
      cy.visit("http://test.test");
      cy.get("a")
        .contains("Laracasts");
    });
  });

describe("Testing login",() => {
    it("Login",() =>{
        cy.visit('http://test.test/login')

        cy.get('input[name=email]').type("miguel-sg00@hotmail.com")
    
        cy.get('input[name=password]').type(`prueba123{enter}`)
    
        cy.url().should('include', '/home')
    
        cy.getCookie('laravel_session').should('exist')
    
        cy.get('a').should('contain', 'miguel')
    });
});