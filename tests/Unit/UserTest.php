<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    

    public function test_user_can_login_with_correct_credentials()
    {
        $user = User::find(1);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'Admin123',
        ]);
        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
    }

    public function test_user_cannot_login_with_incorrect_password()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('i-love-laravel'),
        ]);
        
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'invalid-password',
        ]);
        
        /*$response->assertSessionHasErrors('email');
        $this->assertTrue(session()->hasOldInput('email'));
        $this->assertFalse(session()->hasOldInput('password'));*/
        $this->assertGuest();
    }


    public function test_user_can_view_a_login_form()
    {
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');
    }

    public function test_user_can_register()
    {
        $response = $this->post('/register',[
            'name' => 'Miguel',
            'email' => 'email1@email.com',
            'password' => 'prueba12345',
            'password_confirmation' => 'prueba12345'
        ]);
        $response->assertRedirect('/home');

    }

}
